# Ampt Monitor


## Installation

    cd core
    python setup.py install
    cd ../suricata
    python setup.py install
    

----
## Example config file

    [global]
    utc_offset=0
    monitor_id=1
    logfile='/logs/surilogfile.txt'
    url=http://httpbin.org/post

    [mon1]
    utc_offset=0
    type=suri
    sid=2020721
    path=/logs/filename.txt

##usage

    ampt-run
