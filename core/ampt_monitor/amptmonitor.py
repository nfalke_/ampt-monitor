import time
import logging
import os, pwd, grp
import multiprocessing
import requests

class AmptMonitor(object):
    def __init__(self, monitors, logfile, loglevel, url, monitor_id):
        self.logger = logging.getLogger("AmptMonitor")
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        fh = logging.FileHandler(logfile)
        fh.setLevel(loglevel)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

        ch = logging.StreamHandler()
        ch.setLevel(loglevel)
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

        self.logger.info("logging to {} with loglevel {}".format(logfile, loglevel))
        self.monitors = monitors
        self.url = url
        self.monitor_id = monitor_id

    def _post(self, data):
        return requests.post(self.url, data=data)

    def run(self):
        self.logger.info("Starting monitors")
        for monitor in self.monitors:
            self.logger.info("monitor with path {}".format(monitor.logfile))
            tailer = multiprocessing.Process(target=self._tail, args=(monitor, ))
            processor = multiprocessing.Process(target=self.ampt_process, args=(monitor, ))
            tailer.start()
            processor.start()

    def ampt_process(self, monitor):
        self._drop_privileges()
        while True:
            data = monitor.process()
            if data:
                data["monitor"] = self.monitor_id
                r = self._post(data)
                if r.status_code != 200:
                    self.logger.warning("Non 200 status code")
                    self.logger.warning(r.status_code)
                    try:
                        self.logger.warning(r.text)
                    except:
                        self.logger.warning("no text")

    def _tail(self, monitor, pos=None):
        #get initial position (EOF)
        if pos is None:
            with open(monitor.logfile) as logfile:
                logfile.seek(0, 2)
                pos = logfile.tell()

        #tail file
        while True:
            with open(monitor.logfile) as logfile:
                logfile.seek(0, 2)
                eof = logfile.tell()
                if pos > eof:
                    self.logger.warning("logfile got shorter, this shouldn't happen")
                    pos = eof
                logfile.seek(pos)
                lines = logfile.readlines()
                pos = logfile.tell()
                if lines:
                    for line in lines:
                        monitor.queue.put(line.strip())
                else:
                    time.sleep(.1)

    def _drop_privileges(self, uid_name='nobody', gid_name='nogroup'):
        '''
        drops root privileges from a thread
        '''
        if os.getuid() != 0:
            return
        running_uid = pwd.getpwnam(uid_name).pw_uid
        running_gid = grp.getgrnam(gid_name).gr_gid
        os.setgroups([])
        os.setgid(running_gid)
        os.setuid(running_uid)
