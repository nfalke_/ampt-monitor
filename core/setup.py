import os
from ez_setup import use_setuptools
from setuptools import setup, find_packages
use_setuptools()

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "ampt_monitor",
    version = "0.1",
    author = "Nate Falke",
    author_email = "nate.falke@riskiq.net",
    description = ("core module for ampt monitor"),
    package_data={'': ['LICENSE', 'NOTICE'], 'requests': ['*.pem']},
    package_dir={'ampt_monitor': 'ampt_monitor'},
    install_requires=[
        'configobj',
        'requests',
    ],
    license = "GPLv2",
    keywords = "",
    url = "",
    packages=find_packages(),
#    long_description=read(''),
    classifiers=[
        "Development Status :: 3 - Alpha",
    ],

    entry_points = {
        'console_scripts': [
            'ampt-run = ampt_monitor.cli.amptrun:main'
        ]
    }
)

